package migrations

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
	"humanifyme/start/internal/models"
	"log"
)

func Run(db *gorm.DB, rollbackLast bool) *gorm.DB {
	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{{
		ID: "201608301400",
		Migrate: func(tx *gorm.DB) error {
			return tx.Migrator().CreateTable(&models.User{})
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Migrator().DropTable("users")
		},
	}})

	if err := m.Migrate(); err != nil {
		log.Fatalf("Migration failed: %v", err)
	}

	log.Println("Migration did run successfully")

	if rollbackLast {
		err := m.RollbackLast()
		if err != nil {
			panic("Could not rollback migrations")
		}
	}

	return db
}
