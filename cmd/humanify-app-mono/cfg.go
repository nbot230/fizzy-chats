package main

type Config struct {
	DB struct {
		Port     string `yaml:"port" env-required:"true"`
		Host     string `yaml:"host" env-default:"localhost"`
		Name     string `yaml:"name" env-default:"postgres"`
		User     string `yaml:"user" env-default:"default"`
		Password string `yaml:"pass"`
		Type     string `yaml:"type" env-default:"pg"`
	} `yaml:"db"`
}

type DbTypeError struct {
	error string
}

func (e *DbTypeError) Error() string {
	return e.error
}

func (c *Config) toDbUrl() (string, error) {
	switch c.DB.Type {
	case "pg":
		return "postgres://" + c.DB.User + ":" + c.DB.Password + "@" + c.DB.Host + ":" + c.DB.Port + "/" + c.DB.Name, nil
	default:
		return "", &DbTypeError{error: "Wrong db provider"}
	}
}
