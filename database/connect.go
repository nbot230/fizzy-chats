package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
)

func Conn(dbUrl string) *gorm.DB {
	db, err := gorm.Open(postgres.New(postgres.Config{
		PreferSimpleProtocol: true,
		DSN:                  dbUrl,
	}), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		log.Fatalf("Migration failed: %v", err)
	}

	return db
}
