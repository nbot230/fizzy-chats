package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/ilyakaznacheev/cleanenv"
	_ "github.com/ilyakaznacheev/cleanenv"
	"humanifyme/start/database"
	"humanifyme/start/internal/migrations"
	humanifyme "humanifyme/start/routes"
	"log"
)

func main() {
	cfg := Config{}
	err := cleanenv.ReadConfig("cfg.yml", &cfg)

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Config vars %v:", &cfg)

	dbUrl, err := cfg.toDbUrl()

	if err != nil {
		log.Fatal("You provided wrong DB type")
	}

	db := database.Conn(dbUrl)

	migrations.Run(db, false)

	app := fiber.New()

	humanifyme.Routes(app)

	log.Fatal(app.Listen(":3000"))
}
