package models

import (
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	ID         uint `gorm:"primaryKey"`
	ExternalId uint `gorm:"index:idx_externalid,unique"`
	Name       string
	LastName   string `gorm:"index:idx_phone,unique"`
	Phone      string `gorm:"index:idx_phone,unique"`
	Email      string `gorm:"index:idx_email,unique"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  gorm.DeletedAt `gorm:"index"`
}
